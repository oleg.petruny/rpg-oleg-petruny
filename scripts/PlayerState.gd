extends Node


const MAX_HEALTH = 100

var health = 100
var coins = 0
var position := Vector2.ZERO
var weapon_level = 1

var target_location := Vector2(605, 373)

func increase_health(value: int):
	health = clamp(health + value, 0, MAX_HEALTH)
	HUD.update_health(health)

func decrease_health(value: int):
	health = clamp(health - value, 0, MAX_HEALTH)
	HUD.update_health(health)
	if (health == 0):
		QuestManager.complete()
		QuestManager.complete_count -= 1
		SceneTransition.change_scene("res://scenes/World.tscn")
		await SceneTransition.changed
		health = 100
		HUD.update_health(health)
	
func add_coins(value: int):
	coins += value
	HUD.update_coins(coins - value, coins)
