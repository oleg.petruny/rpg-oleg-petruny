extends StaticBody2D

var is_active := false

func _on_CloseArea_area_entered(area):
	if !area.is_in_group("Player"):
		return
	is_active = true
	update_text()
	$CanvasLayer/AnimationPlayer.play("InfoPanelShow")
	$CanvasLayer2/Control/AnimationPlayer.play("Show")

func _on_CloseArea_area_exited(area):
	if area.is_in_group("Player"):
		is_active = true
		$CanvasLayer/AnimationPlayer.play_backwards("InfoPanelShow")
		$CanvasLayer2/Control/AnimationPlayer.play_backwards("Show")

func update_text():
	if QuestManager.completed:
		if QuestManager.complete_count == 0:
			$CanvasLayer/TextureRect/Label.text = "Hello newcomchacho! Mi casa su casa! Wanna try some quest?"
		elif QuestManager.complete_count < 3:
			$CanvasLayer/TextureRect/Label.text = "Not bad, not bad. Some more quest?"
		else:
			$CanvasLayer/TextureRect/Label.text = "Very good. But I think you can do more..."
	else:
		$CanvasLayer/TextureRect/Label.text = "Be careful chacho. Outside is an enemy camp waiting for annihilation."

func _unhandled_input(event):
	if event.is_action_pressed("Action") and is_active and QuestManager.completed:
		QuestManager.generate()
		update_text()
