extends Node

var locations :Array = [
	Vector2(240, 500), Vector2(470, 560), Vector2(820, 520), 
	Vector2(740, 270), Vector2(530, 190), Vector2(325, 250),
	]
var camp_prefabs :Array = [
	preload("res://scenes/quests/camps/campEasy.tscn"),
	preload("res://scenes/quests/camps/campMedium.tscn"),
	preload("res://scenes/quests/camps/campHard.tscn"),
	preload("res://scenes/quests/camps/campBoss.tscn"),
	]
	
var enemy_prefab = preload("res://scenes/Enemy.tscn")
var coin_prefabs = [
	preload("res://scenes/CoinsSmall.tscn"),
	preload("res://scenes/CoinsMedium.tscn"),
	preload("res://scenes/CoinsLarge.tscn"),
	preload("res://scenes/CoinsLargeUltra.tscn"),
	]

var complete_count := 0
var completed := true
var instance = null
var location: Vector2

func generate():
	completed = false
	var location = locations[randi_range(0, len(locations) - 1)]
	var camp = camp_prefabs[randi_range(0, len(camp_prefabs) - 1)]
	instance = camp.instantiate()
	instance.position = location
	HUD.quest_update()

func complete():
	instance = null
	completed = true
	complete_count += 1
	HUD.quest_update()

func fill_enemies(parent :Node2D, positions :Array, max_level :int):
	for i in positions:
		var enemy = enemy_prefab.instantiate()
		enemy.init(randi_range(1, max_level))
		parent.add_child(enemy)
		enemy.position = i

func fill_coins(offset :Vector2, positions :Array, small := true, medium := true, large := true, ultra := true):
	for i in positions:
		var size = randi_range(1, 4)
		var coin
		if (size > 3 and ultra):
			coin = coin_prefabs[3].instantiate()
		elif (size > 2 and large):
			coin = coin_prefabs[2].instantiate()
		elif (size > 1 and medium):
			coin = coin_prefabs[1].instantiate()
		elif (size > 0 and small):
			coin = coin_prefabs[0].instantiate()
		else:
			coin = coin_prefabs[3].instantiate()
		get_tree().current_scene.add_child(coin)
		coin.position = i + offset
	
