extends StaticBody2D

@export var price_step := 100
@export var weapon :Node2D

var is_active := false
var price := 0

func _ready():
	weapon = weapon.get_node("Weapon")

func _unhandled_input(event):
	if event.is_action_pressed("Action") and is_active:
		if PlayerState.coins >= price:
			weapon.increase_level()
			PlayerState.add_coins(-price)
			update_price()

func update_price():
	if (weapon.level < 5):
		price = (weapon.level * weapon.level) * price_step
		$CanvasLayer/TextureRect/Label.text = "Hola muchacho! For weapon upgrade you need [" + str(price) + "] coins."
	else:
		$CanvasLayer/TextureRect/Label.text = "I don't have any sword left muchacho!"

func _on_CloseArea_area_entered(area):
	if area.is_in_group("Player"):
		is_active = true
		update_price()
		$CanvasLayer/AnimationPlayer.play("InfoPanelShow")
		$CanvasLayer2/Control/AnimationPlayer.play("Show")

func _on_CloseArea_area_exited(area):
	if area.is_in_group("Player"):
		is_active = false
		$CanvasLayer/AnimationPlayer.play_backwards("InfoPanelShow")
		$CanvasLayer2/Control/AnimationPlayer.play_backwards("Show")
