extends Area2D


@export var next_scene: String = "res://scenes/House.tscn"
@export var message: String = "Press SPACE to enter."
@export var target_location: Vector2

var is_active = false

func _ready():
	$CanvasLayer/Control/Label.text = message

func _unhandled_input(event):
	if event.is_action_pressed("Action") and is_active:
		SceneTransition.change_scene(next_scene)
		PlayerState.target_location = target_location


func _on_Teleport_area_entered(area):
	if area.is_in_group("Player"):
		is_active = true
		$CanvasLayer/Control/AnimationPlayer.play("Show")


func _on_Teleport_area_exited(area):
	if area.is_in_group("Player"):
		is_active = false
		$CanvasLayer/Control/AnimationPlayer.play_backwards("Show")
