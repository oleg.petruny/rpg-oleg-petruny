extends Node2D

signal changed

func change_scene(next_scene: String):
	$CanvasLayer/ColorRect/AnimationPlayer.play("fade")
	await $CanvasLayer/ColorRect/AnimationPlayer.animation_finished
	get_tree().change_scene_to_file(next_scene)
	$CanvasLayer/ColorRect/AnimationPlayer.play_backwards("fade")
	changed.emit()
