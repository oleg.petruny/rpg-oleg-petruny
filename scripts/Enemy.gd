extends Area2D

@export var level := 1
@export var health := 25
@export var damage := 5

var dead := false
var knock := false
var move_tween = null
var loot = preload("res://scenes/CoinsMedium.tscn")

func _ready():
	init(level)

func init(level :int):
	self.level = level
	$LabelLevel.text = "^".repeat(level)
	health *= level
	$BarHealth.max_value = health
	$BarHealth.value = $BarHealth.max_value
	damage *= level

func get_hurt(value :int):
	if (move_tween != null):
		move_tween.stop()
		move_tween = null
		
	position -= (PlayerState.position - position).normalized() * 8
	$TimerMove.start()
	
	health = clamp(health - value, 0, $BarHealth.max_value)
	$BarHealth.value = health
	$FloatingTextManager.show_value(str(value))
	
	if (health == 0):
		dead = true
		queue_free()
		var drop = loot.instantiate()
		get_tree().current_scene.add_child(drop)
		drop.position = global_position
		if (get_parent().name.contains("camp")):
			get_parent().kill()
		return
	
	$Sprite2D.modulate = Color.RED
	$TimerGetHurt.start()
	await $TimerGetHurt.timeout
	$Sprite2D.modulate = Color.WHITE

func attack():
	if (!dead):
		PlayerState.decrease_health(damage)

func _on_area_entered(area):
	if (area.is_in_group("Player")):
		area.get_hurt(damage)
	elif(area.is_in_group("Weapon")):
		get_hurt(area.damage);

func move():
	move_tween = create_tween()
	var direction = (PlayerState.position - global_position).normalized()
	move_tween.tween_property(self, "position", position + direction * 16, 0.25).set_trans(Tween.TRANS_CUBIC).set_ease(Tween.EASE_IN_OUT)
	move_tween.tween_callback(func(): move_tween = null)

func _on_timer_move_timeout():
	move()

func _on_area_vision_area_entered(area):
	if (area.is_in_group("Player")):
		move()
		$TimerMove.start()

func _on_area_vision_area_exited(area):
	if (area.is_in_group("Player")):
		$TimerMove.stop()
