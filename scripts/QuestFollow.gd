extends Control

@export var border := Vector2(30, 30)
@export var size_curve: Curve
@export var size_curve_duration := 1.0

var size_curve_time := 0.0

func _process(delta):
	if (QuestManager.completed || !get_tree().current_scene.name.contains("World")):
		return
	
	size_curve_time += delta
	if (size_curve_time > size_curve_duration):
		size_curve_time -= size_curve_duration
	scale = Vector2.ONE * size_curve.sample(size_curve_time)
	
	var quest :Vector2 = QuestManager.instance.global_position
	
	var player :Vector2 = PlayerState.position
	rotation = (quest - player).angle() - PI/2
	var viewport :Vector2 = get_viewport().size
	var camera_zoom = get_viewport().get_camera_2d().zoom
	quest *= camera_zoom.x
	player *= camera_zoom.x
	var target_position = (quest - player + viewport/2).clamp(border, viewport - border)
	position = lerp(position, target_position, delta * 10)
