extends CanvasLayer

var coin_tween :Tween
var health_tween :Tween

func _ready():
	quest_update()

func update_coins(old :float, new :float):
	if coin_tween != null and coin_tween.is_running():
		coin_tween.stop()
	coin_tween = create_tween()
	coin_tween.tween_method(func(value):$VBoxContainer/Control/Coins.text = "Coins: " + str(ceil(value)), old, new, 0.5) \
		.set_trans(Tween.TRANS_LINEAR)

func update_health(health):
	if health_tween != null and health_tween.is_running():
		health_tween.stop()
	health_tween = create_tween()
	health_tween.tween_property($VBoxContainer/ProgressBar, "value", health, 0.2) \
		.set_trans(Tween.TRANS_CUBIC).set_ease(Tween.EASE_IN_OUT)

func quest_update():
	$QuestFollow.visible = !QuestManager.completed
	$VBoxContainer/HBoxContainer/LabelQuestState.text = "COMPLETED" if QuestManager.completed else "IN PROGRESS"
	$VBoxContainer/HBoxContainer/LabelQuestState.modulate = Color(0.5, 1, 0.5) if QuestManager.completed else Color(1, 0.5, 0.5)
