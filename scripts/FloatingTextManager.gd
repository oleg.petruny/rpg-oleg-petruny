extends Node2D

var FloatingText = preload("res://scenes/FloatingText.tscn")

@export var travel := Vector2(0, -14)
@export var spread := PI/2
@export var duration := 0.7

func show_value(value :String):
	var text = FloatingText.instantiate()
	add_child(text)
	text.show_value(value, travel, spread, duration)
