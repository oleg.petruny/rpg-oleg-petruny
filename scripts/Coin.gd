extends Area2D

@export var price :int
@export var size_curve :Curve
@export var size_curve_duration := 1.0

var size_curve_time := size_curve_duration * randf()
func _process(delta):
	size_curve_time += delta
	if (size_curve_time > size_curve_duration):
		size_curve_time -= size_curve_duration
	scale = Vector2.ONE * size_curve.sample(size_curve_time)
