extends Node2D

@export var enemies :Array = []
@export var coins :Array = []
@export var max_level := 1
@export var small := true
@export var medium := true
@export var large := true
@export var ultra := true
@export var boss := false

func _ready():
	QuestManager.fill_enemies(self, enemies, max_level)
	QuestManager.fill_coins(self.position, coins, small, medium, large, ultra)
	if (boss):
		enemies.append(Vector2.ZERO)

func kill():
	enemies.pop_back()
	if (len(enemies) == 0):
		QuestManager.complete()
		queue_free()
