extends Area2D

@export var speed: float = 4
signal moved

const TILE_SIZE = 16

const inputs = {
	"Left": Vector2.LEFT,
	"Right": Vector2.RIGHT,
	"Up": Vector2.UP,
	"Down": Vector2.DOWN
}

var last_dir = Vector2.ZERO
var last_action = ""
var move_tween: Tween
var in_shop := false

func _ready():
	# align position to the middle of a tile
	position.x = int(position.x / TILE_SIZE) * TILE_SIZE
	position.y = int(position.y / TILE_SIZE) * TILE_SIZE
	position += Vector2.ONE * TILE_SIZE/2
	# set timer interval according to the speed
	$MoveTimer.wait_time = 1.0/speed
	position = PlayerState.target_location
	PlayerState.position = position
	

func _unhandled_input(event):
	for action in inputs:
		if event.is_action_pressed(action):
			var dir = inputs[action]
			if move_tile(dir):
				# repeat the action in fixed intervals, if it is still pressed
				last_action = action
				last_dir = dir
				$MoveTimer.start()


func move_tile(direction: Vector2):
	if move_tween != null:
		return
	
	$RayCast2D.target_position = direction * TILE_SIZE
	$RayCast2D.force_raycast_update()
	if !$RayCast2D.is_colliding():
		move_tween = create_tween()
		move_tween.tween_property(self, "position", position + direction * TILE_SIZE, 1 / (speed + 2)) \
		.set_trans(Tween.TRANS_CUBIC).set_ease(Tween.EASE_IN_OUT)
		moved.emit()
		PlayerState.position = global_position + direction * TILE_SIZE
		move_tween.tween_callback(func(): move_tween = null)
		return true
	
	var other = $RayCast2D.get_collider()
	if other.has_method("on_collision"):
		other.on_collision(self)
	
	return false

func _on_MoveTimer_timeout():
	if Input.is_action_pressed(last_action):
		if move_tile(last_dir):  # do the same move as the last time
			return
	# reset
	last_action = ""
	last_dir = Vector2.ZERO
	$MoveTimer.stop()

func get_hurt(value):
	PlayerState.decrease_health(value)
	$FloatingTextManager.show_value(str(value))
	get_viewport().get_camera_2d().shake()
	
	$Sprite2D.modulate = Color.RED
	$GetHurtTimer.start()
	await $GetHurtTimer.timeout
	$Sprite2D.modulate = Color.WHITE

func collect_coins(value):
	PlayerState.add_coins(value)

func _on_area_entered(area):
	if (area.is_in_group("Coins")):
		print("Collected price: ", area.price)
		collect_coins(area.price)
		area.queue_free()
