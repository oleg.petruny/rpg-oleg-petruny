extends Area2D

@export var damage_step := 5

var level := 1
var damage := damage_step

func _ready():
	var temp = PlayerState.weapon_level
	while level < temp:
		increase_level()

func _process(delta):
	var mouse = get_viewport().get_mouse_position()
	var viewport :Vector2 = get_viewport().size
	
	rotation = (mouse - viewport/2).angle()

func increase_level():
	if (level == 5):
		return
	
	level += 1
	PlayerState.weapon_level = level
	damage = damage_step * level
	var rect = $Sprite2D.region_rect
	rect.position.x += 17
	$Sprite2D.set_region_rect(rect)
